var app = new Vue({
    el: "#root",
    delimiters: ["${","}"],
    data: {
        favorites: [],
        categories: [],
        categoryFavorites: [],
        logs: [],
        loading: true,
        currentFavorite: {},
        currentCategory: {},
        message: null,
        newFavorite: { "title": null, "description": "", "metadata": {}, "ranking": null, "category": null},
        newCategory: {"name": null},
    },
    created() {
        this.getFavorites();
        this.getCategories();
    },
    methods: {
        getFavorites()
        {
            this.loading = true;
            this.$http.get('/api/favorites/')
                .then((response) => {
                    this.favorites = response.data.sort(dynamicSort('ranking'));
                    console.log(this.favorites);
                    this.loading = false;
                })
                .catch((err) => {
                    this.loading = false;
                    console.log(err);
                })
        },
        getFavorite(id) {
            this.loading = true;
            this.$http.get(`/api/favorites/${id}/`)
                .then((response) => {
                    this.currentFavorite = response.data;
                    this.getCategory(this.currentFavorite.category.id);
                    this.currentFavorite.category = this.currentFavorite.category.name;
                    this.loading = false;
                })
                .catch((err) => {
                    this.loading = false;
                    console.log(err);
                })
        },
        editFavorite(id) {
            this.currentFavorite = {};
            this.getFavorite(id);
            $("#showCategoryFavoritesModal").modal('hide');
            $("#showFavoriteModal").modal("hide");
            $("#editFavoriteModal").modal("show");
        },
        showFavorite(id) {
            this.currentFavorite = {};
            this.getFavorite(id);
            $("#editFavoriteModal").modal("hide");
            $("#showFavoriteModal").modal("show");
        },
        addFavorite() {
            this.loading = true;
            this.$http.post('/api/favorites/', this.newFavorite)
                .then((response) => {
                    console.log(response.data.status);
                    this.loading = false;
                    this.getFavorites();
                    this.getCategories();
                    $("#addFavoriteModal").modal('hide');
                })
                .catch((err) => {
                    this.loading = false;
                    console.log(err);
                })
        },
        updateFavorite() {
            this.loading = true;
            this.$http.put(`/api/favorites/${this.currentFavorite.id}/`, this.currentFavorite)
                .then((response) => {
                    this.loading = false;
                    this.currentFavorite = response.data.data;
                    this.currentFavorite.category = this.currentFavorite.category.name;
                    this.getFavorites();
                    $("#editFavoriteModal").modal('hide');
                })
                .catch((err) => {
                    this.loading = false;
                    console.log(err);
                })
        },
        getCategories() {
            this.loading = true;
            this.$http.get('/api/categories/')
                .then((response) => {
                    this.categories = response.data;
                    this.loading = false;
                })
                .catch((err) => {
                    this.loading = false;
                    console.log(err);
                })
        },
        getCategory(id) {
            this.loading = true;
            this.$http.get(`/api/categories/${id}/`)
                .then((response) => {
                    this.currentCategory = response.data;
                    this.loading = false;
                })
                .catch((err) => {
                    this.loading = false;
                    console.log(err);
                })
        },
        addCategory() {
            this.loading = true;
            this.$http.post('/api/categories/', this.newCategory)
                .then((response) => {
                    console.log(response.data.error);
                    this.getCategories();
                    this.getFavorites();
                    $("#addCategoryModal").modal('hide');
                    this.loading = false;
                })
                .catch((err) => {
                    this.loading = false;
                    console.log(err);
                })
        },
        showCategoryFavorites(id) {
            this.loading = true;
            this.$http.get(`/api/categories/${id}/favorites`)
                .then((response) => {
                    this.currentCategory = {};
                    this.getCategory(id);
                    this.categoryFavorites = response.data.sort(dynamicSort("ranking"));
                    $("#showCategoryFavoritesModal").modal('show');
                    this.loading = false;
                })
                .catch((err) => {
                    this.loading = false;
                    console.log(err);
                })
        },
        viewLogs() {
            this.loading = true;
            this.$http.get('/api/logs/')
                .then((response) => {
                    this.logs = response.data.sort(dynamicSort("-id"));
                    $("#viewLogModal").modal('show');
                    this.loading = false;
                })
                .catch((err) => {
                    this.loading = false;
                    console.log(err);
                })
        }
    }
});

function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        /* next line works with strings and numbers,
         * and you may want to customize it to your needs
         */
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}
