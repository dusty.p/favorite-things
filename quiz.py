from cryptography.fernet import Fernet

key = "TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM="

# Oh no! The code is going over the edge! What are you going to do?
message = (
    b"gAAAAABdLqc_AWS_WUHq0ouoqo4jUeSbsX7WT-sxTms9vGUea-y7XYb6ao_Vj"
    b"54TZMfJ71EzEJgSRHz1z09tgFW0QE8GuBpuLoV--Q05e7UNbKEZ4h2R6SglUu"
    b"o0uPkU5KFlJlx3w_jyuie2bfMKvAeyn_WwnVqYuYdDiTOWNRNJ2ghtAOzHts8="
)


def main():
    f = Fernet(key)
    print(f.decrypt(message))


if __name__ == "__main__":
    main()
