**How long did you spend on the coding test below?**

I built the API over the course of 4 days. In total I spent about 10 hours over 
those 4 days. The Vue.js frontend took me about 6 hours over 2 days to complete 
as this was the first project I have done in Vue.js. Adding the unit tests took 
approximately 6 hours.

---
**What would you add to your solution if you had more time?**

If I had more time I would add several things.
* User authentication and individualized data
* More robust data checking
* Dynamic Key/Value data entry in the frontend for the metadata field
* A cleaner UI/UX
* More user feedback in the UI
* Add features to the Log model with foreign keys to the favorite/category that
was modified.

---

**What was the most useful feature that was added to the latest version of your 
chosen language?**

Python 3.7 added dataclasses to the standard lib. These make it much easier to 
make an object for easy referencing of data using dot notation without having to
 write a bunch of boilerplate methods. Below is a simple example of how I used 
the @dataclass decorator.
 
```python
from dataclasses import dataclass

@dataclass
class Asset:
    asset_no: str
    asset_type: str
    model: str
    serial_number: str
    po_number: str
    warranty_type: str

    def get_csv(self):
        return f"{self.asset_no}," \
               f"{self.asset_type}," \
               f"{self.model}," \
               f"{self.serial_number}," \
               f"{self.po_number}," \
               f"{self.warranty_type}"

# elsewhere in the code
asset = Asset(asset_no=sn, serial_number=sn, po_number=po, **defaults[model])
out += "\n" + asset.get_csv()
```

This is a very simplified example and I have removed much of the implementation 
details that make this the preferred method instead of just using the information
directly.

---

**How would you track down performance issues in production?**

I would start by monitoring the performance metrics using something like 
Graphite. Using proper monitoring I should be able to determine what process is 
causing the performance issues. I can then dig into the code and with the help
of strategic logging find where the issue is in the project.

---

**Have you ever had to do this?**

I have had to do this a couple times, mostly for memory usage issues in python 
projects. Converting a few things to weakrefs solved the issue in both cases.