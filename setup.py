import sys
from setuptools import setup

install_requires = [
    'Django >=2.2.3',
    'django_compressor >=2.1',
    'django-environ',
    'django-prometheus',
    'psycopg2 >=2.8',
    'djangorestframework >= 3.0'
]
if sys.version_info[0] < 3:
    install_requires.append('django-health-check <3')
else:
    install_requires.append('django-health-check')

setup(
    name='BriteCore',
    version='0.1',
    packages=['BriteCore', 'favorite_things', 'favorite_things.migrations'],
    url='',
    license='',
    author='dustinpianalto',
    author_email='dustin@djpianalto.com',
    description='',
    install_requires=install_requires
)
