FROM praekeltfoundation/django-bootstrap

COPY . /app/
RUN pip install -e .

ENV DJANGO_SETTINGS_MODULE BriteCore.settings
ENV SUPERUSER_PASSWORD test

RUN django-admin collectstatic --noinput

CMD ["BriteCore.wsgi:application"]
