from django.apps import AppConfig


class RestApiConfig(AppConfig):
    name = 'favorite_things'
