from favorite_things.models import Log


def log_action(description: str) -> Log:
    if len(description) > 200:
        raise RuntimeError("Log message is too long.")

    log = Log(description=description)
    log.save()

    return log
