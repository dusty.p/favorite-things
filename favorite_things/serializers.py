from rest_framework import serializers
from favorite_things.models import Category
from favorite_things.models import Log
from favorite_things.models import Favorite


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "name"]


class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Log
        fields = ["id", "datetime", "description"]


class FavoriteSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Favorite
        fields = [
            "id",
            "title",
            "description",
            "ranking",
            "metadata",
            "category",
            "created",
            "modified",
        ]

