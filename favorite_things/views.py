from django.shortcuts import render, redirect
from django.views import View
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.response import Response
from favorite_things.models import Favorite
from favorite_things.models import Category
from favorite_things.models import Log
from favorite_things.serializers import FavoriteSerializer
from favorite_things.serializers import LogSerializer
from favorite_things.serializers import CategorySerializer
import json
from favorite_things.utils import log_action

API_ERROR = {"status": "Failed", "error": ""}

API_SUCCESS = {"status": "Successful", "data": {}}

TEST = {"title": "foo", "ranking": 1, "category": "foodly"}


def generate_error(msg: str = "There was a problem with your query"):
    response = API_ERROR
    response["error"] = msg
    return response


def generate_success_response(data: dict):
    response = API_SUCCESS
    response["data"] = data
    return response


def check_duplicate_rankings(ranking: int, category: int, count: int = 0) -> int:
    try:
        duplicate_rank = Favorite.objects.filter(category=category).get(ranking=ranking)
    except ObjectDoesNotExist:
        return count
    else:
        duplicate_rank.ranking += 1
        count = check_duplicate_rankings(duplicate_rank.ranking, category, count + 1)
        duplicate_rank.save()
        return count


class APIBase(View):
    def get(self, request, format=None):
        return redirect("./favorites", request, format=format)


class FavoritesList(APIView):
    def get(self, request, format=None):
        favorites = [FavoriteSerializer(fav).data for fav in Favorite.objects.all()]
        return Response(favorites)

    def post(self, request, format=None):
        data = request.data
        title = data.get("title", None)
        description = data.get("description", "")
        ranking = data.get("ranking", None)
        metadata = data.get("metadata", {})
        category = data.get("category", None)

        if not (title and ranking and category):
            return Response(
                generate_error("title, ranking, and category are required fields")
            )

        try:
            ranking = int(ranking)
        except ValueError:
            return Response(generate_error("ranking must be a number greater than 0"))

        if ranking <= 0:
            return Response(generate_error("ranking must be a number greater than 0"))

        try:
            db_cat = Category.objects.get(name=category.title())
        except ObjectDoesNotExist:
            return Response(generate_error(f"Category {category} does not exist."))

        rankings_updated = check_duplicate_rankings(ranking, db_cat.id)

        favorite = Favorite(
            title=title,
            description=description,
            ranking=ranking,
            metadata_string=json.dumps(metadata),
            category=db_cat,
        )

        favorite.save()
        log_action(
            f"Favorite added: {title} as rank {ranking} in category {db_cat.name}\n"
            f"{rankings_updated} favorites had to have their rankings adjusted."
        )

        response = generate_success_response(FavoriteSerializer(favorite).data)
        return Response(response)


class FavoritesDetail(APIView):
    def get(self, request, pk, format=None):
        try:
            favorite = Favorite.objects.get(id=pk)
        except ObjectDoesNotExist:
            return Response(generate_error(f"ID {pk} does not exist."))
        else:
            serialized_favorite = FavoriteSerializer(favorite)
            return Response(serialized_favorite.data)

    def put(self, request, pk, format=None):
        data = request.data
        title = data.get("title", None)
        description = data.get("description", None)
        ranking = data.get("ranking", None)
        metadata = data.get("metadata", None)
        category = data.get("category", None)

        try:
            favorite = Favorite.objects.get(id=pk)
        except ObjectDoesNotExist:
            return Response(generate_error(f"ID {pk} does not exist."))

        favorite.title = title or favorite.title
        favorite.description = description or favorite.description
        favorite.ranking = ranking or favorite.ranking
        favorite.metadata_string = json.dumps(metadata or favorite.metadata)

        if category:
            db_cat = None
            try:
                db_cat = Category.objects.get(name=category.title())
            except ObjectDoesNotExist:
                return Response(generate_error(f"Category {category} does not exist."))

            favorite.category = db_cat or favorite.category

        rankings_updated = check_duplicate_rankings(
            ranking or favorite.ranking, favorite.category.id
        )

        favorite.save()
        log_action(
            f"Favorite updated: {favorite.title} as rank {favorite.ranking} in category {favorite.category.name}"
            + (
                f"\n{rankings_updated} favorites had to have their rankings adjusted."
                if rankings_updated
                else ""
            )
        )

        response = generate_success_response(FavoriteSerializer(favorite).data)
        return Response(response)


class CategoryList(APIView):
    def get(self, request, format=None):
        categories = [CategorySerializer(cat).data for cat in Category.objects.all()]
        return Response(categories)

    def post(self, request, format=None):
        data = request.data
        name = data.get("name", None)

        if not name:
            return Response(generate_error("A name for the category is required."))

        try:
            db_cat = Category.objects.get(name=name.title())
        except ObjectDoesNotExist:
            category = Category(name=name.title())

            category.save()
            log_action(f"Category Added: {category.name}")

            return Response(
                generate_success_response(CategorySerializer(category).data)
            )
        else:
            return Response(
                generate_error(f"The category {name.title()} already exists.")
            )


class CategoryDetail(APIView):
    def get(self, request, pk, format=None):
        category = Category.objects.get(id=pk)
        serialized_category = CategorySerializer(category)
        return Response(serialized_category.data)


class LogList(APIView):
    def get(self, request, format=None):
        logs = [LogSerializer(log).data for log in Log.objects.all()]
        return Response(logs)


class LogDetail(APIView):
    def get(self, request, pk, format=None):
        log = Log.objects.get(id=pk)
        serialized_log = LogSerializer(log)
        return Response(serialized_log.data)


class CategoryFavoritesList(APIView):
    def get(self, request, pk, format=None):
        try:
            category = Category.objects.get(id=pk)
        except ObjectDoesNotExist:
            return Response(generate_error("That category does not exist."))
        else:
            favorites = Favorite.objects.filter(category=category)
            if favorites:
                serialized_favorites = [
                    FavoriteSerializer(fav).data for fav in favorites
                ]
                return Response(serialized_favorites)
            else:
                return Response(
                    generate_error("That category does not have any favorites.")
                )
