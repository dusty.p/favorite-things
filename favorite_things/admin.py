from django.contrib import admin

# Register your models here.
from favorite_things.models import Favorite
from favorite_things.models import Category
from favorite_things.models import Log

admin.site.register(Favorite)
admin.site.register(Category)
admin.site.register(Log)
