from collections import OrderedDict

from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase
from django.test import Client
from django.urls import reverse
import json
from datetime import datetime

from favorite_things.models import Log
from favorite_things.models import Favorite
from favorite_things.models import Category
from favorite_things.views import generate_error
from favorite_things.views import generate_success_response
from favorite_things.views import check_duplicate_rankings
from favorite_things.views import APIBase
from favorite_things.views import FavoritesList
from favorite_things.views import FavoritesDetail
from favorite_things.views import CategoryList
from favorite_things.views import CategoryDetail
from favorite_things.views import CategoryFavoritesList
from favorite_things.views import LogDetail
from favorite_things.views import LogList
from favorite_things.serializers import FavoriteSerializer
from favorite_things.serializers import CategorySerializer
from favorite_things.serializers import LogSerializer
from favorite_things.utils import log_action

# Create your tests here.


class FavoriteTest(TestCase):
    @staticmethod
    def create_favorite(
        title="Test", description="", ranking=1, category="Person", metadata="{}"
    ):
        return Favorite.objects.create(
            title=title,
            description=description,
            ranking=ranking,
            category=Category.objects.get(name=category),
            metadata_string=metadata,
        )

    def test_favorite_creation(self):
        f = self.create_favorite()
        self.assertTrue(isinstance(f, Favorite))
        self.assertEqual(str(f), f.title)
        self.assertEqual(f.metadata, json.loads(f.metadata_string))

    @classmethod
    def setUpTestData(cls):
        p = Category(name="Person")
        p.save()


class CategoryTest(TestCase):
    @staticmethod
    def create_category(name="Test"):
        return Category.objects.create(name=name)

    def test_category_creation(self):
        c = self.create_category()
        self.assertTrue(isinstance(c, Category))
        self.assertEqual(str(c), c.name)

    def tearDown(self) -> None:
        Category.objects.all().delete()


class LogTest(TestCase):
    @staticmethod
    def create_log_entry(description="Something was did"):
        return Log.objects.create(description=description)

    def test_log_creation(self):
        l = self.create_log_entry()
        self.assertTrue(isinstance(l, Log))
        self.assertEqual(str(l), f"{l.datetime} | {l.description}")


class UtilsTest(TestCase):
    def test_error_creation(self):
        error_msg = "Some Error"
        error = generate_error(error_msg)
        self.assertEqual(error["status"], "Failed")
        self.assertEqual(error["error"], error_msg)

    def test_success_creation(self):
        data = {"key": "value"}
        suc = generate_success_response(data=data)
        self.assertEqual(suc["status"], "Successful")
        self.assertEqual(suc["data"], data)

    def test_check_duplicate_rankings(self):
        self.assertEqual(Favorite.objects.get(title="Test1").ranking, 1)
        self.assertEqual(Favorite.objects.get(title="Test2").ranking, 2)
        count = check_duplicate_rankings(1, Category.objects.get(name="Test").id)
        self.assertEqual(count, 2)
        self.assertEqual(Favorite.objects.get(title="Test1").ranking, 2)
        self.assertEqual(Favorite.objects.get(title="Test2").ranking, 3)

    def test_log_action(self):
        self.assertRaises(RuntimeError, log_action, description="a" * 201)
        log_msg = "Test Log"
        log = log_action(description=log_msg)
        self.assertTrue(isinstance(log, Log))
        self.assertEqual(str(Log.objects.get(id=log.id)), str(log))

    @classmethod
    def setUpTestData(cls):
        cat = Category(name="Test")
        cat.save()
        Favorite.objects.create(title="Test1", ranking=1, category=cat).save()
        Favorite.objects.create(title="Test2", ranking=2, category=cat).save()


class ViewsGetTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Category.objects.all().delete()
        Favorite.objects.all().delete()
        Log.objects.all().delete()
        Category(name="Test_empty").save()
        cat = Category(name="Test")
        cat.save()
        fav1 = Favorite(
            title="Test1",
            description="Test Object Number 1",
            ranking=1,
            category=cat,
            metadata_string="{}",
        )
        fav1.save()
        fav2 = Favorite(
            title="Test2",
            description="Test Object Number 2",
            ranking=2,
            category=cat,
            metadata_string='{"test":"foo"}',
        )
        fav2.save()
        Log(description="Category Test created").save()
        Log(description="Favorite Test1 created").save()
        Log(description="Favorite Test2 created").save()

    def test_get_favorites(self):
        c = Client()
        response = c.get("/api/favorites.json")
        self.assertEqual(response.status_code, 200)
        fav1 = {}
        fav2 = {}
        cat_obj = Category.objects.get(name="Test")
        cat = OrderedDict([("id", cat_obj.id), ("name", cat_obj.name)])
        for d in response.data:
            if d["title"] == "Test1":
                fav1 = d
            elif d["title"] == "Test2":
                fav2 = d
        self.assertEqual(len(response.data), 2)
        self.assertEqual(fav1["ranking"], 1)
        self.assertEqual(fav1["category"], cat)
        self.assertEqual(fav1["metadata"], {})
        self.assertEqual(fav1["description"], "Test Object Number 1")
        self.assertEqual(fav2["ranking"], 2)
        self.assertEqual(fav2["category"], cat)
        self.assertEqual(fav2["metadata"], {"test": "foo"})
        self.assertEqual(fav2["description"], "Test Object Number 2")

    def test_get_favorite_by_id(self):
        c = Client()
        cat_obj = Category.objects.get(name="Test")
        cat = OrderedDict([("id", cat_obj.id), ("name", cat_obj.name)])
        response_fail = c.get("/api/favorites/0.json")
        self.assertEqual(response_fail.status_code, 200)
        self.assertEqual(response_fail.data["status"], "Failed")
        ids = [fav["id"] for fav in c.get("/api/favorites.json").data]
        for i in ids:
            response_suc = c.get(f"/api/favorites/{i}.json")
            self.assertEqual(response_suc.status_code, 200)
            self.assertEqual(response_suc.data["id"], i)
            self.assertEqual(response_suc.data["category"], cat)

    def test_get_categories(self):
        c = Client()
        response = c.get("/api/categories.json")
        self.assertEqual(response.status_code, 200)
        cats = Category.objects.all()
        cat_dicts = [{"id": cat.id, "name": cat.name} for cat in cats]
        for resp in response.data:
            self.assertTrue(resp in cat_dicts)

    def test_get_category(self):
        c = Client()
        cat = Category.objects.get(name="Test")
        response = c.get(f"/api/categories/{cat.id}.json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["name"], cat.name)

    def test_get_logs(self):
        c = Client()
        response = c.get("/api/logs.json")
        self.assertEqual(response.status_code, 200)
        logs = Log.objects.all()
        logs_list = [
            {
                "id": log.id,
                "datetime": log.datetime.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                "description": log.description,
            }
            for log in logs
        ]
        self.assertEqual(response.data, logs_list)

    def test_get_log(self):
        c = Client()
        logs = Log.objects.all()
        ids = [log.id for log in logs]
        for i in ids:
            response = c.get(f"/api/logs/{i}.json")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.data["id"], i)

    def test_get_category_favorites(self):
        c = Client()
        resp_fail = c.get("/api/categories/0/favorites")
        self.assertEqual(resp_fail.status_code, 200)
        self.assertEqual(resp_fail.data["status"], "Failed")

        cat_empty = Category.objects.get(name="Test_empty")
        resp_fail2 = c.get(f"/api/categories/{cat_empty.id}/favorites")
        self.assertEqual(resp_fail2.status_code, 200)
        self.assertEqual(resp_fail2.data["status"], "Failed")

        cat = Category.objects.get(name="Test")
        favs = Favorite.objects.filter(category=cat.id)
        fav_ids = [f.id for f in favs]
        response = c.get(f"/api/categories/{cat.id}/favorites")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), len(favs))
        for fav in favs:
            self.assertEqual(fav.category.id, cat.id)
            self.assertTrue(fav.id in fav_ids)


class ViewsPostTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Category.objects.all().delete()
        Favorite.objects.all().delete()
        Log.objects.all().delete()
        Category(name="Test").save()

    def test_favorite_post(self):
        c = Client()
        cat = Category.objects.get(name="Test")
        post1 = {
            "title": "Test1",
            "ranking": 1,
            "category": cat.name,
            "description": "Testing 1...2...3...",
            "metadata": '{"test1": "foo"}',
        }
        post2 = {
            "title": "Test2",
            "ranking": 1,
            "category": cat.name,
            "description": "Testing 1...2...3...4...",
            "metadata": '{"test": "foo2"}',
        }
        for post in [post1, post2]:
            response = c.post("/api/favorites.json", post)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.data["status"], "Successful")
            self.assertEqual(response.data["data"]["title"], post["title"])
            self.assertEqual(response.data["data"]["description"], post["description"])
            self.assertEqual(response.data["data"]["metadata"], post["metadata"])
            self.assertEqual(response.data["data"]["ranking"], post["ranking"])
            fav = Favorite.objects.get(id=response.data["data"]["id"])
            self.assertEqual(fav.title, post["title"])
            self.assertEqual(
                fav.created.strftime("%Y-%m-%dT%H:%M:%S.%f"),
                response.data["data"]["created"],
            )

        favs = Favorite.objects.all()
        self.assertEqual(len(favs), 2)
        self.assertNotEqual(favs[0].ranking, favs[1].ranking)

        # Test without all the required arguments
        bad_post = {"title": "test"}
        resp_fail = c.post("/api/favorites.json", bad_post)
        self.assertEqual(resp_fail.status_code, 200)
        self.assertEqual(resp_fail.data["status"], "Failed")

        # Test with a negative ranking
        bad_post = {"title": "test", "ranking": -1, "category": cat.name}
        resp_fail = c.post("/api/favorites.json", bad_post)
        self.assertEqual(resp_fail.status_code, 200)
        self.assertEqual(resp_fail.data["status"], "Failed")

        # Test with a character instead of a number for ranking
        bad_post = {"title": "test", "ranking": "a", "category": cat.name}
        resp_fail = c.post("/api/favorites.json", bad_post)
        self.assertEqual(resp_fail.status_code, 200)
        self.assertEqual(resp_fail.data["status"], "Failed")

        # Test with a bad category
        bad_post = {"title": "test", "ranking.json": 10, "category": cat.name + "_bad"}
        resp_fail = c.post("/api/favorites.json", bad_post)
        self.assertEqual(resp_fail.status_code, 200)
        self.assertEqual(resp_fail.data["status"], "Failed")

    def test_category_post(self):
        c = Client()
        cat_add = {"name": "Test_post"}
        response = c.post("/api/categories.json", cat_add)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["status"], "Successful")
        self.assertEqual(response.data["data"]["name"], cat_add["name"].title())

        # This should fail because a name is required
        cat_bad = {}
        resp_fail = c.post("/api/categories.json", cat_bad)
        self.assertEqual(resp_fail.status_code, 200)
        self.assertEqual(resp_fail.data["status"], "Failed")

        # This should fail because a category with this name already exists
        cat_bad = {"name": "Test_post"}
        resp_fail = c.post("/api/categories.json", cat_bad)
        self.assertEqual(resp_fail.status_code, 200)
        self.assertEqual(resp_fail.data["status"], "Failed")


class ViewsUpdateTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Category.objects.all().delete()
        Favorite.objects.all().delete()
        Log.objects.all().delete()
        cat = Category(name="Test")
        cat.save()
        Favorite(title="Test", ranking=1, category=cat).save()

    def test_favorite_put(self):
        c = Client(content_type="application/json")
        cat = Category.objects.get(name="Test")
        fav = Favorite.objects.get(title="Test")
        new_fav = {"title": "Test_updated", "ranking": 2}
        response = c.put(
            f"/api/favorites/{fav.id}.json",
            data=new_fav,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["status"], "Successful")
        self.assertEqual(response.data["data"]["title"], new_fav["title"])
        self.assertEqual(response.data["data"]["ranking"], new_fav["ranking"])
        self.assertGreater(
            response.data["data"]["modified"], response.data["data"]["created"]
        )
        self.assertEqual(response.data["data"]["category"]["id"], cat.id)

        # This should fail because that favorite id should not exist
        resp_fail = c.put(
            f"/api/favorites/{fav.id + 100}.json",
            new_fav,
            content_type="application/json",
        )
        self.assertEqual(resp_fail.status_code, 200)
        self.assertEqual(resp_fail.data["status"], "Failed")

        # This should fail because that category name doesn't exist
        bad_fav = {"title": "Test_updated", "ranking": 2, "category": cat.name + "_bad"}
        resp_fail = c.put(
            f"/api/favorites/{fav.id}.json", bad_fav, content_type="application/json"
        )
        self.assertEqual(resp_fail.status_code, 200)
        self.assertEqual(resp_fail.data["status"], "Failed")


class RedirectTest(TestCase):
    def test_api_home_redirect(self):
        c = Client()
        response = c.get("/api/")
        self.assertEqual(response.status_code, 302)
