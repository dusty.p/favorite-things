from django.db import models
import json

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Log(models.Model):
    datetime = models.DateTimeField(auto_now=True)
    description = models.TextField(max_length=200)

    def __str__(self):
        return f"{self.datetime} | {self.description}"


class Favorite(models.Model):
    title = models.TextField(max_length=20, null=False)
    description = models.TextField(max_length=500)
    ranking = models.IntegerField(null=False)
    metadata_string = models.TextField(blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=False)
    created = models.DateTimeField(auto_now_add=True, null=False)
    modified = models.DateTimeField(auto_now=True, null=False)

    def __str__(self):
        return self.title

    @property
    def metadata(self):
        try:
            return json.loads(self.metadata_string)
        except:
            return {}
