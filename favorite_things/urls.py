from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from favorite_things import views
from django.views.generic import TemplateView

urlpatterns = [
    path(r"api/", views.APIBase.as_view()),
    path(r"api/favorites/", views.FavoritesList.as_view()),
    path(r"api/favorites/<int:pk>/", views.FavoritesDetail.as_view()),
    path(r"api/categories/", views.CategoryList.as_view()),
    path(r"api/categories/<int:pk>/", views.CategoryDetail.as_view()),
    path(r"api/logs/", views.LogList.as_view()),
    path(r"api/logs/<int:pk>/", views.LogDetail.as_view()),
    path(r"api/categories/<int:pk>/favorites", views.CategoryFavoritesList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])

urlpatterns.append(path(r"", TemplateView.as_view(template_name="index.html")))
