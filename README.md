# Favorite Things

A place to list your favorite things.

## Usage

This app is available at [https://favorites.djpianalto.com](https://favorites.djpianalto.com)

The usage is fairly straight forward, click the `Add Favorite` to add one of 
your favorite things. The `Title`, `Ranking`, and `Category` fields are required
 everything else is optional.

If you would like to add a category click the `Add Category` button and fill in 
a name for your category.

To view all the changes that have been made click the `View Log` button.

To view the details of a favorite just click anywhere on said favorite's listing
 to open a popup to show the details. You can edit the favorite in 2 places, 
either using the `Edit` button to the right of the favorite's listing or by 
using the `Edit` button in the Favorite's details popup. When editing a favorite
 no fields are required.

To view all of the favorites in a specific category just click on the name of 
the category.

## API


The api base url is `/api`

#### Endpoints

##### `/favorites/`
    `get` : Returns a list of all the favorites
    
    `post` : Adds the favorite included in the request body. `title`, `ranking`,
    and `category` are required.
##### `/favorites/<id>/`
    `get` : Returns a dict of the favorite specified by `<id>`
    
    `put` : Updates the favorite specified by `<id>` with the data in the 
    request body.
##### `/categories/`
    `get` : Returns a list of all the categories
    
    `post` : Adds a new category using the data in the request body. `name` is 
    required.
##### `/categories/<id>/`
    `get` : Returns a dict of the category specified by `<id>`
##### `/categories/<id>/favorites`
    `get` : Returns a list of all the favorites in the category specified by `<id>`
##### `/logs/`
    `get` : Returns a list of all the log entries
##### `/logs/<id>/`
    `get` : Returns a dict of the log entry specified by `<id>`

## Deployment

Due to financial constraints I was not able to use AWS or Google Cloud Services 
to host my project. As such it is currently running on one of my personal 
servers at home. I used Docker to create a container for the PostgreSQL database
 and another one for the web application. The Django project is running using 
Gunicorn to serve the web pages and Nginx as a proxy and static file server. 
This is all also behind the Nginx reverse proxy container that I use to provide 
subdomain routing and LetsEncrypt certificates for all my web apps.

I have configured Gitlab CI testing on the repo so my test suite is run 
automatically whenever I push a commit to the remote repo.

Due to my deployment strategy I am not able to fully automate the deployment 
process but I do use git on the server to pull any changes from the Gitlab repo 
and then I can rebuild the docker container. One project I am working on is 
building a  Kubernetes cluster which will allow me to further automate this 
deployment system and scale my web apps as needed. I also need to modify my 
docker container to use volumes so I don't have to rebuild the container every 
time the code changes. For this project it was not a big deal because I will 
most likely not be making many changes to the app once deployed but would be 
necessary in a full production app.

The Secret key and database connection info are passed in through environment 
variables to prevent leaking sensitive info to a public git repo.